package cl.unab.triangulo.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cl.unab.triangulo.Triangulo;

class PruebasUnitarias {

	@Test
	void testEquilatero() {
		Triangulo equilatero = new Triangulo(5,5,5);
		assertEquals("Equil�tero", equilatero.tipoTriangulo());
	}
	
	@Test
	void testEscaleno() {
		Triangulo escaleno = new Triangulo(1,2,3);
		assertEquals("Escaleno", escaleno.tipoTriangulo());
	}
	
	@Test
	void testIsocelesAB() {
		Triangulo isoceles = new Triangulo(1,1,9);
		assertEquals("Is�celes", isoceles.tipoTriangulo());
	}
	
	@Test
	void testIsocelesAC() {
		Triangulo isoceles = new Triangulo(1,9,1);
		assertEquals("Is�celes", isoceles.tipoTriangulo());
	}
	
	@Test
	void testIsocelesBC() {
		Triangulo isoceles = new Triangulo(9,1,1);
		assertEquals("Is�celes", isoceles.tipoTriangulo());
	}
	
	@Test
	void testExiste() {
		Triangulo existe = new Triangulo(3,4,5);
		assertEquals(true, existe.existeTriangulo());
	}
	
	@Test
	void testNoExiste() {
		Triangulo existe = new Triangulo(0,4,5);
		assertEquals(false, existe.existeTriangulo());
	}
	
}
