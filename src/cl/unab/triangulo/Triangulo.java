package cl.unab.triangulo;

public class Triangulo {
	private float ladoA;
	private float ladoB;
	private float ladoC;
	
	public Triangulo(float ladoA, float ladoB, float ladoC) {
		super();
		this.ladoA = ladoA;
		this.ladoB = ladoB;
		this.ladoC = ladoC;
	}

	public String tipoTriangulo() {
		if(ladoA == ladoB && ladoB == ladoC) {
			return "Equil�tero";
		} else if(ladoA != ladoB && ladoA != ladoC && ladoB != ladoC) {
			return "Escaleno";
		} else return "Is�celes";
	}
	
	public boolean existeTriangulo() {
		if(ladoA + ladoB > ladoC && ladoA + ladoC > ladoB && ladoB + ladoC > ladoA) {
			return true;
		} else {
			return false;
		}
	}
}
